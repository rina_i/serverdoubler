#include <QApplication>
#include <servermainwindow.h>

#include <iostream>
#include <QThread>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    ServerMainWindow mw;
    mw.show();
    mw.setWindowTitle("Server");
    mw.move(100, 100);
    mw.resize(100, 100);
    return a.exec();
}
