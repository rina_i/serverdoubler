#include "serverworker.h"

#include <QThread>


//#define DEBUG_SERVERWORKER
#ifdef DEBUG_SERVERWORKER
#include <iostream>
#endif

bool getNumber(const QByteArray& pack, double& num){
    bool ok;
    num = pack.toDouble(&ok);
    return ok;
}

ServerWorker::ServerWorker(int sockDescriptor,
                           TcpState* st):
    QRunnable(),
    sockDescr(sockDescriptor),
    needRun(false),
    tmout(30000),
    state(st)
{}

ServerWorker::~ServerWorker()
{
    stop();
    delete state;
    // delete sock;
}

void ServerWorker::run(){
    QTcpSocket sock;
    //Установка дескриптора
    if (!sock.setSocketDescriptor(sockDescr)){
        stop();
        setError(TcpState::SocketBindFailed);
        return;
    }
    setState(TcpState::Connected);
//Дескриптор установлен, настройка сокета
    //Устанавливаем флаг в true
    needRun = true;
    const uint maxLen = 1000;
    //Установка буфера на чтение
    sock.setReadBufferSize(maxLen);
    while(needRun && sock.state() == QAbstractSocket::ConnectedState){
        //Ожидание готовности на чтение
        if (!sock.waitForReadyRead(tmout)){
            //Превышен таймаут ожидания
            stop();
#ifdef DEBUG_SERVERWORKER
            std::cout << "Err: " << sock.errorString().toStdString()
                      << " " << sock.bytesAvailable() << std::endl;
#endif
            if (sock.state() == QAbstractSocket::ConnectedState){
                setError(TcpState::TimeoutExpired);
            }else{
                setError(TcpState::ConnectionClosed);
            }
            //sock.disconnectFromHost();
            return;
        }
        if (!needRun){
            stop();
            return;
        }
#ifdef DEBUG_SERVERWORKER
        std::cout << "Bytes availible " << sock.bytesAvailable() << std::endl;
#endif
        if (sock.bytesAvailable() <= 0)
            continue;
        //Считывание данных
        QByteArray pack(sock.read(sock.bytesAvailable()));
        double val;
        //Пытаемся распознать пришедшую строку как число
        if (getNumber(pack, val)){
            //Ок, строка распознана, пишем ответ
            if (sock.write(QByteArray::number(val*2)) == -1
                    || !sock.waitForBytesWritten(100)){
                //Не удалось записать ответ
#ifdef DEBUG_SERVERWORKER
                std::cout << "Error while sending " << val*2 << std::endl;
#endif
                stop();
                setError(TcpState::WriteError);
                return;
            }
            //Ок, данные записаны, ждем следующий пакет
#ifdef DEBUG_SERVERWORKER
            std::cout << "Sended answer: " << val*2 << std::endl;
#endif
        }else{
            //Пришедшая строка - не число
            stop();
#ifdef DEBUG_SERVERWORKER
            std::cout << "Not a number: " << QString(pack).toStdString() << std::endl;
#endif
            setError(TcpState::NotNumber);
            return;
        }
    }
    //Вышли из цикла. Если затребован stop через команду. надо закрыть соединение
    if (sock.state() == QTcpSocket::ConnectedState){
        sock.disconnectFromHost();
        setState(TcpState::Disconnected);
    }else{
        setError(TcpState::ConnectionClosed);
    }
}

void ServerWorker::stop(){
    needRun = false;
}



