#ifndef SERVERWOKER_H
#define SERVERWOKER_H

#include <QRunnable>
#include <QTcpSocket>
#include <tcpstate.h>


class ServerWorker : public QRunnable{

public:
    /**
     * @brief ServerWorker Создаем новый поток выполнения для процесса-клиента
     * @param socketDescr Дескриптор сокета
     * @param state Состояние потока. Уничтожается при вызове деструктора
     */
    explicit ServerWorker(int sockDescriptor, TcpState* state);
    ~ServerWorker();
    /**
     * @brief setTimeout Установка таймаута ожидания
     * @param msec время ожидания, в миллисекундах
     */
    void setTimeout(unsigned msec){
        tmout = msec;
    }

    /**
     * @brief getDescriptor Возвращает текущий дескриптор
     * @return дескриптор сокета
     */
    int getDescriptor() const{
        return sockDescr;
    }

    /**
     * @brief run Перегруженный метод
     */
    void run() Q_DECL_OVERRIDE;
    /**
     * @brief stop Вызывается для завершения работы потока.
     */
    void stop();
private:
    int sockDescr; //Дескриптор сокета
    volatile bool needRun; //Флаг продолжения работы/остановки
    unsigned tmout; //Таймаут ожидания, в миллисекундах
    TcpState* state; //Класс, хранящий состояние соединения

    /**
     * @brief setState Установка состояния в state
     * @param st устанавливаемое состояние
     */
    void setState(TcpState::State st){
        if (state == 0
                || state == nullptr)
            return;
        state->setState(st);
    }

    /**
     * @brief setError Устанавливает состояние state в Error и устанавливает код ошибки
     * @param err Устанавливаемая ошибка
     */
    void setError(TcpState::Errors err){
        if (state == 0
                || state == nullptr){
            return;
        }
        state->setError(err);
        state->setState(TcpState::Error);
    }
};

#endif // SERVERWOKER_H
