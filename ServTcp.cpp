#include "ServTcp.h"

#include <iostream>
#include <QDateTime>



ServTCP::ServTCP(QObject* parent):
    QObject(parent),
    serv(new QTcpServer()),
    tmout(30000)
{
    connect(serv, SIGNAL(newConnection()),
            this, SLOT(hasNewConnection()));
}

ServTCP::~ServTCP()
{
    emit stopAllWorkers();
    serv->close();;
}

bool ServTCP::startListening(int port){
    //проверка корректности порта
    if (port < 0)
        return false;
    //Проверка, запущен ли уже сервер
    if (!serv->isListening()){
        //Открытие сервера на прослушивание
        serv->listen(QHostAddress::Any, port);
        //Возвращаем состояние прослушивания
        return serv->isListening();
    }
    //Сервер уже запущен - по новой его не запустили
    return false;
}

void ServTCP::stopListening(){
    emit stopAllWorkers();
    serv->close();
}

void ServTCP::hasNewConnection(){
    //Создаем отдельного воркера для нового соединения
    QTcpSocket* sock = serv->nextPendingConnection();
    TcpState* state = new TcpState();
    ServerWorker* worker = new ServerWorker(sock->socketDescriptor(), state);
    worker->setTimeout(tmout);

    //Изменнения состояния воркера - через класс-состояние.
    //Можно было сделать множественное наследование у ServerWorker, но пусть будет так
    //Вообще, если надо будет доделать обработку ошибок, то обрабатывать сигнал о смене
    //состояния надо иначе. Пока проверяется только подключено или нет.
    connect(state, &TcpState::stateChanged, [this, worker, state](int st)->void{
        if (st == TcpState::Connected){
            emit connectionEstablished(worker->getDescriptor(), st);
        }else{
            emit connectionClosed(worker->getDescriptor(), st, state->getError());
        }
            });
    //По сигналу остановки всё должно прекратиться
    connect(this, &ServTCP::stopAllWorkers,
            [this, worker]()->void{
                worker->stop();
    });
    //Запускаем нить на работу
    QThreadPool::globalInstance()->start(worker);
}

