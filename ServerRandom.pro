#-------------------------------------------------
#
# Project created by QtCreator 2015-04-24T21:39:40
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ServerRandom
QMAKE_CXXFLAGS += -std=c++0x

TEMPLATE = app


SOURCES += main.cpp \
    serverworker.cpp \
    servermainwindow.cpp \
    ServTcp.cpp \
    tcpstate.cpp

HEADERS += \
    serverworker.h \
    servermainwindow.h \
    ServTcp.h \
    tcpstate.h

FORMS += \
    servermainwindow.ui
