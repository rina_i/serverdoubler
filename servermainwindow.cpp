#include "servermainwindow.h"
#include "ui_servermainwindow.h"

#include <QThread>


ServerMainWindow::ServerMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ServerMainWindow),
    serv(new ServTCP())
{
    ui->setupUi(this);
    this->centralWidget()->setLayout(ui->layCentral);
    ui->gbSettings->setLayout(ui->laySettings);


    connect(ui->bStart, &QPushButton::clicked,
            [this]()-> void{
                serv->setTimeout(ui->sbTmout->value());
                serv->startListening(ui->sbPort->value());
                lockInputWidgets(true);
            });

    connect(ui->bStop, &QPushButton::clicked,
            [this]() -> void{
        serv->stopListening();
        lockInputWidgets(false);
    });

    lockInputWidgets(false);

}

ServerMainWindow::~ServerMainWindow()
{
    delete ui;
}

void ServerMainWindow::lockInputWidgets(bool isLock){
    ui->gbSettings->setEnabled(!isLock);
    ui->bStart->setEnabled(!isLock);
    ui->bStop->setEnabled(isLock);
}
