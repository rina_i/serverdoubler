#ifndef SERVTCP_H
#define SERVTCP_H

#include <QTcpServer>
#include <QThreadPool>
#include <QTimer>
#include <QMutex>

#include <serverworker.h>


class ServTCP : public QObject
{
    Q_OBJECT
public:
    ServTCP(QObject* parent = 0);
    ~ServTCP();
    bool isListening(){
        return serv->isListening();
    }
    void setTimeout(unsigned msec){
        tmout = msec;
    }

signals:
    /**
     * @brief stopAllWorkers emit to stop all worker's threads
     */
    void stopAllWorkers();
    void connectionEstablished(int sockDescr, int state);
    void connectionClosed(int sockaddr, int state, int err);
public slots:
    /**
     * @brief hasNewConnection new connection handling;
     *  for each connection making new thread
     */
    void hasNewConnection();\
    /**
     * @brief start Starting server with given port number;
     * if port is incorrect (i.e. < 0)
     * or port number is busy by another application,
     * or if server is alredy started,
     * or if any errors with binding, return false
     * otherwise return true.
     * @param port Number of TCP port
     * @return true, if successfully open; false otherwise;
     */
    bool startListening(int port);
    /**
     * @brief stop Close all current connections
     * and shut down server
     */
    void stopListening();
private:
    //Qt server
    QTcpServer* serv;
    //Время ожидания ответа
    unsigned tmout;
};

#endif // SERVTCP_H
