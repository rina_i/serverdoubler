#ifndef TCPSTATE_H
#define TCPSTATE_H

#include <QObject>

class TcpState : public QObject
{
    Q_OBJECT
public:
    enum State{
        Connected,
        Disconnected,
        Error
    };
    enum Errors{
        NoError,
        SocketBindFailed,
        TimeoutExpired,
        ConnectionClosed,
        NotNumber,
        WriteError
    };

    explicit TcpState(QObject *parent = 0);
    void setState(State state){
        if (this->state != state){
            this->state = state;
            emit stateChanged(state);
        }
    }
    void setError(Errors errNum){
        this->errNum = errNum;
    }
    int getError() const{
        return errNum;
    }

signals:
    void stateChanged(int state);
private:
    int errNum;
    int state;
    
};

#endif // TCPSTATE_H
