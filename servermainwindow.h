#ifndef SERVERMAINWINDOW_H
#define SERVERMAINWINDOW_H

#include <QMainWindow>
#include <ServTcp.h>

namespace Ui {
class ServerMainWindow;
}

class ServerMainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit ServerMainWindow(QWidget *parent = 0);
    ~ServerMainWindow();
    
private:
    Ui::ServerMainWindow *ui;
    ServTCP* serv;
    void lockInputWidgets(bool isLock = true);
};

#endif // SERVERMAINWINDOW_H
